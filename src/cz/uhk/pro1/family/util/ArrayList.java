package cz.uhk.pro1.family.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E> {

    private Object[] elements = new Object[0];

    @Override
    public void add(E element) {
        Object[] newElements = new Object[elements.length + 1];
        copyArrayElements(elements, 0, newElements, 0, elements.length);
        newElements[newElements.length - 1] = element;
        elements = newElements;
        // výkonově by nemuselo být ideální
    }

    @Override
    public void add(E element, int index) {
        if (index < 0 || index > elements.length) {
            throw new IndexOutOfBoundsException();
        }

        Object[] newElements = new Object[elements.length + 1];
        copyArrayElements(elements,0,newElements,0,index-1);
        newElements[index] = element;
        copyArrayElements(elements,index,newElements,index +1,(elements.length - index));

        elements = newElements;
    }

    @Override
    public void remove(int index) {
        if (index < 0 || index > (elements.length)) {
            throw new IndexOutOfBoundsException();
        }

        Object[] newElements = new Object[elements.length - 1];
        copyArrayElements(elements,0,newElements,0,index);
        copyArrayElements(elements,(index + 1),newElements,index,(newElements.length - index));

        elements = newElements;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= elements.length) {
            throw new IndexOutOfBoundsException();
        }
        return (E) elements[index];
    }

    @Override
    public int size() {
        return elements.length;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    private void copyArrayElements(Object[] src, int srcPos, Object[] dest, int destPos, int length) {
        for (int i = 0; i < length; i++) {
            dest[destPos + i] = src[srcPos + i];
        }
    } // System.arraycopy(src, srcPos, dest, destPos, length);

    private class ArrayListIterator implements Iterator<E> {

        private int cursor;

        @Override
        public boolean hasNext() {
            return cursor < elements.length;
        }

        @Override
        public E next() {
            if (cursor >= elements.length) {
                throw new NoSuchElementException();
            }
            return (E) elements[cursor++];
        }

    }

}
