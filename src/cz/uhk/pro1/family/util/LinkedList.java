package cz.uhk.pro1.family.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<E> implements List<E> {

    private Node first;
    private int size; // aby se velikost nemusela vždy dopočítávat, tj. procházet celý seznam

    @Override
    public void add(E element) {
        if (first == null) {
            first = new Node(element, null);
        } else {
            Node node = first;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node(element, null);
        }
        size++;
    }

    @Override
    public void add(E element, int index) {
        if(index > size){
            throw new IndexOutOfBoundsException();
        }

        Node lastNode = first;
        for(int i = 0; i < (index -1); i++){
            lastNode = lastNode.next;
        }

        Node New = new Node(element,lastNode.next);
        lastNode.next = New;
        if(index == 0){
            first = New;
        }
        size++;
    }

    @Override
    public void remove(int index) {
        if(index > size -1){
            throw new IndexOutOfBoundsException();
        }
        Node currentNode = first;
        for(int i = 1; i < index; i++){
            currentNode = currentNode.next;
        }
        if(index == 0){
            first = currentNode.next;
        }else{
            currentNode.next = currentNode.next.next;
        }
        size--;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = first;
        for (int i = 0; i < index; i++){
            node = node.next;
        }
        return node.item;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }

    private class LinkedListIterator implements Iterator<E> {

        private int cursor;
        private Node nextNode = first;

        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        @Override
        public E next() {
            if (cursor >= size) {
                throw new NoSuchElementException();
            }
            E currentItem = nextNode.item;
            nextNode = nextNode.next;
            cursor++;
            return currentItem;
        }

    }

    private class Node {

        private E item;
        private Node next;

        Node(E item, Node next) {
            this.item = item;
            this.next = next;
        }

    }

}
