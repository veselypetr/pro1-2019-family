package cz.uhk.pro1.family.services;

import cz.uhk.pro1.family.model.Family;

public class RevenueOffice implements Office {

    @Override
    public String check(Family f) {
        int sum = 0;
        if (f.getMother() != null) {
            sum += f.getMother().getSalary();
        }
        if (f.getFather() != null) {
            sum += f.getFather().getSalary();
        }
        if (f.getChildren().size() > 0) {
            double avg = (double) sum / f.getChildren().size();
            return "celkový příjem rodičů je " + sum + " a průměrný příjem na dítě je " + avg;
        } else {
            return "celkový příjem rodičů je " + sum;
        }
    }

}
