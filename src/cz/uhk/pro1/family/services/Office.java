package cz.uhk.pro1.family.services;

import cz.uhk.pro1.family.model.Family;

public interface Office {

    public String check(Family f);

}
