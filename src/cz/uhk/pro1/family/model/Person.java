package cz.uhk.pro1.family.model;

public class Person {

    private String givenName;
    private String surname;
    private int salary;

    public Person(String givenName, String surname) {
        this.givenName = givenName;
        this.surname = surname;
//         this(givenName, surname, 0);
    }

    public Person(String givenName, String surname, int salary) {
        this.givenName = givenName;
        this.surname = surname;
        this.salary = salary;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return givenName + " " + surname;
    }

}
