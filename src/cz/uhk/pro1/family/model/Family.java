package cz.uhk.pro1.family.model;

import cz.uhk.pro1.family.util.ArrayList;
//import cz.uhk.pro1.family.util.LinkedList;
import cz.uhk.pro1.family.util.List;

public class Family {

    private Person mother;
    private Person father;
    private List<Person> children = new ArrayList<>();
//    private List<Person> children = new LinkedList<>();

    public Family(Person mother, Person father) {
        this.mother = mother;
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public Person getFather() {
        return father;
    }

    public List<Person> getChildren() {
        return children;
    }

}
