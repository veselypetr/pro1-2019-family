package cz.uhk.pro1.family.tests;

import cz.uhk.pro1.family.model.Person;
import cz.uhk.pro1.family.util.LinkedList;
import cz.uhk.pro1.family.util.List;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test pro kontrolu domácího úkolu
 */
public class LinkedListTest {

    private List<Person> people = new LinkedList<>();
    private Person person0 = new Person("Leonard", "Hofstadter");
    private Person person1 = new Person("Sheldon", "Cooper");
    private Person person2 = new Person("Howard", "Wolowitz");
    private Person person = new Person("Raj", "Koothrappali");

    @Before
    public void setUp() throws Exception {
        people.add(person0);
        people.add(person1);
        people.add(person2);
    }

    @Test
    public void addFirst() {
        people.add(person, 0);
        assertEquals(person, people.get(0));
        assertEquals(4, people.size());
    }

    @Test
    public void addMiddle() {
        people.add(person, 1);
        assertEquals(person, people.get(1));
        assertEquals(4, people.size());
    }

    @Test
    public void addLast() {
        people.add(person, 3);
        assertEquals(person, people.get(3));
        assertEquals(4, people.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addException() {
        people.add(person, 4);
    }

    @Test
    public void removeFirst() {
        people.remove(0);
        assertEquals(person1, people.get(0));
        assertEquals(2, people.size());
    }

    @Test
    public void removeMiddle() {
        people.remove(1);
        assertEquals(person2, people.get(1));
        assertEquals(2, people.size());
    }

    @Test
    public void removeLast() {
        people.remove(2);
        assertEquals(person1, people.get(1));
        assertEquals(2, people.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeException() {
        people.remove(3);
    }

}