package cz.uhk.pro1.family.app;

import cz.uhk.pro1.family.model.Family;
import cz.uhk.pro1.family.model.Person;
import cz.uhk.pro1.family.services.Office;
import cz.uhk.pro1.family.services.RevenueOffice;
import cz.uhk.pro1.family.services.SocialWelfareOffice;
import cz.uhk.pro1.family.services.VitalRecordsOffice;

import cz.uhk.pro1.family.util.List;

public class FamilyApp {

    public static void main(String[] args) {

        Person homer = new Person("Homer", "Simpson", 40000);
        Person marge = new Person("Marge", "Simpsonová", 0);
        Person bart = new Person("Bart", "Simpson");
        Person lisa = new Person("Líza", "Simpsonová");
        Person maggie = new Person("Maggie", "Simpsonová");

        Family simpsons = new Family(marge, homer);
        List<Person> simpsonsChildrenList = simpsons.getChildren();
        simpsonsChildrenList.add(bart);
        simpsonsChildrenList.add(lisa);
        simpsonsChildrenList.add(maggie);

        Office office = new SocialWelfareOffice();
        System.out.println(office.check(simpsons));

        office = new VitalRecordsOffice();
        System.out.println(office.check(simpsons));

        office = new RevenueOffice();
        System.out.println(office.check(simpsons));

    }

}
